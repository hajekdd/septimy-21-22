﻿namespace RomanNumbers
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.errorMessage = new System.Windows.Forms.Label();
            this.test = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxBinary = new System.Windows.Forms.TextBox();
            this.labelBinary = new System.Windows.Forms.Label();
            this.buttonBinary = new System.Windows.Forms.Button();
            this.buttonBinary2 = new System.Windows.Forms.Button();
            this.labelBinary2 = new System.Windows.Forms.Label();
            this.labelDecimalEasy = new System.Windows.Forms.Label();
            this.buttonDecimalEasy = new System.Windows.Forms.Button();
            this.buttonDecimal = new System.Windows.Forms.Button();
            this.labelDecimal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(240, 77);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(264, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(279, 142);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Převeď";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(341, 224);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // errorMessage
            // 
            this.errorMessage.AutoSize = true;
            this.errorMessage.ForeColor = System.Drawing.Color.Red;
            this.errorMessage.Location = new System.Drawing.Point(237, 109);
            this.errorMessage.Name = "errorMessage";
            this.errorMessage.Size = new System.Drawing.Size(0, 13);
            this.errorMessage.TabIndex = 3;
            // 
            // test
            // 
            this.test.AutoSize = true;
            this.test.Location = new System.Drawing.Point(56, 357);
            this.test.Name = "test";
            this.test.Size = new System.Drawing.Size(24, 13);
            this.test.TabIndex = 5;
            this.test.Text = "test";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(376, 142);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "FromRoman";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBoxBinary
            // 
            this.textBoxBinary.Location = new System.Drawing.Point(13, 77);
            this.textBoxBinary.Name = "textBoxBinary";
            this.textBoxBinary.Size = new System.Drawing.Size(100, 20);
            this.textBoxBinary.TabIndex = 7;
            // 
            // labelBinary
            // 
            this.labelBinary.AutoSize = true;
            this.labelBinary.Location = new System.Drawing.Point(12, 147);
            this.labelBinary.Name = "labelBinary";
            this.labelBinary.Size = new System.Drawing.Size(35, 13);
            this.labelBinary.TabIndex = 8;
            this.labelBinary.Text = "binary";
            // 
            // buttonBinary
            // 
            this.buttonBinary.Location = new System.Drawing.Point(13, 109);
            this.buttonBinary.Name = "buttonBinary";
            this.buttonBinary.Size = new System.Drawing.Size(75, 23);
            this.buttonBinary.TabIndex = 9;
            this.buttonBinary.Text = "binary";
            this.buttonBinary.UseVisualStyleBackColor = true;
            this.buttonBinary.Click += new System.EventHandler(this.buttonBinary_Click);
            // 
            // buttonBinary2
            // 
            this.buttonBinary2.Location = new System.Drawing.Point(94, 109);
            this.buttonBinary2.Name = "buttonBinary2";
            this.buttonBinary2.Size = new System.Drawing.Size(75, 23);
            this.buttonBinary2.TabIndex = 10;
            this.buttonBinary2.Text = "binary easy";
            this.buttonBinary2.UseVisualStyleBackColor = true;
            this.buttonBinary2.Click += new System.EventHandler(this.buttonBinary2_Click);
            // 
            // labelBinary2
            // 
            this.labelBinary2.AutoSize = true;
            this.labelBinary2.Location = new System.Drawing.Point(91, 147);
            this.labelBinary2.Name = "labelBinary2";
            this.labelBinary2.Size = new System.Drawing.Size(41, 13);
            this.labelBinary2.TabIndex = 11;
            this.labelBinary2.Text = "binary2";
            // 
            // labelDecimalEasy
            // 
            this.labelDecimalEasy.AutoSize = true;
            this.labelDecimalEasy.Location = new System.Drawing.Point(91, 224);
            this.labelDecimalEasy.Name = "labelDecimalEasy";
            this.labelDecimalEasy.Size = new System.Drawing.Size(49, 13);
            this.labelDecimalEasy.TabIndex = 15;
            this.labelDecimalEasy.Text = "decimal2";
            // 
            // buttonDecimalEasy
            // 
            this.buttonDecimalEasy.Location = new System.Drawing.Point(94, 186);
            this.buttonDecimalEasy.Name = "buttonDecimalEasy";
            this.buttonDecimalEasy.Size = new System.Drawing.Size(85, 23);
            this.buttonDecimalEasy.TabIndex = 14;
            this.buttonDecimalEasy.Text = "decimal easy";
            this.buttonDecimalEasy.UseVisualStyleBackColor = true;
            this.buttonDecimalEasy.Click += new System.EventHandler(this.buttonDecimalEasy_Click);
            // 
            // buttonDecimal
            // 
            this.buttonDecimal.Location = new System.Drawing.Point(13, 186);
            this.buttonDecimal.Name = "buttonDecimal";
            this.buttonDecimal.Size = new System.Drawing.Size(75, 23);
            this.buttonDecimal.TabIndex = 13;
            this.buttonDecimal.Text = "decimal";
            this.buttonDecimal.UseVisualStyleBackColor = true;
            this.buttonDecimal.Click += new System.EventHandler(this.buttonDecimal_Click);
            // 
            // labelDecimal
            // 
            this.labelDecimal.AutoSize = true;
            this.labelDecimal.Location = new System.Drawing.Point(12, 224);
            this.labelDecimal.Name = "labelDecimal";
            this.labelDecimal.Size = new System.Drawing.Size(43, 13);
            this.labelDecimal.TabIndex = 12;
            this.labelDecimal.Text = "decimal";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelDecimalEasy);
            this.Controls.Add(this.buttonDecimalEasy);
            this.Controls.Add(this.buttonDecimal);
            this.Controls.Add(this.labelDecimal);
            this.Controls.Add(this.labelBinary2);
            this.Controls.Add(this.buttonBinary2);
            this.Controls.Add(this.buttonBinary);
            this.Controls.Add(this.labelBinary);
            this.Controls.Add(this.textBoxBinary);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.test);
            this.Controls.Add(this.errorMessage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label errorMessage;
        private System.Windows.Forms.Label test;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxBinary;
        private System.Windows.Forms.Label labelBinary;
        private System.Windows.Forms.Button buttonBinary;
        private System.Windows.Forms.Button buttonBinary2;
        private System.Windows.Forms.Label labelBinary2;
        private System.Windows.Forms.Label labelDecimalEasy;
        private System.Windows.Forms.Button buttonDecimalEasy;
        private System.Windows.Forms.Button buttonDecimal;
        private System.Windows.Forms.Label labelDecimal;
    }
}

