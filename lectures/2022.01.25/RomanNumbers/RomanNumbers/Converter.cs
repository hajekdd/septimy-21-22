﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumbers
{
    class Converter : IRomanConverter, IBinaryConverter
    {
        private readonly Dictionary<char, int>  RomanToNumber = new Dictionary<char, int>
        {
            { 'I', 1 },
            { 'V', 5 },
            { 'X', 10 },
            { 'L', 50 },
            { 'C', 100 },
            { 'D', 500 },
            { 'M', 1000 },
        };

        private readonly Dictionary<int, string>  NumberToRoman = new Dictionary<int, string>
        {
            { 1000, "M" },
            { 900, "CM" },
            { 500, "D" },
            { 400, "CD" },
            { 100, "C" },
            { 90, "XC" },
            { 50, "L" },
            { 40, "XL" },
            { 10, "X" },
            { 9, "IX" },
            { 5, "V" },
            { 4, "IV" },
            { 1, "I" },
        };

        public int FromRoman(string roman)
        {
            int total = 0;
            char previousRoman = roman[0];
            char currentRoman;

            for (int i = 0; i < roman.Length; i++)
            {
                currentRoman = roman[i];

                if (RomanToNumber[currentRoman] > RomanToNumber[previousRoman])
                    total += RomanToNumber[currentRoman] - (2 * RomanToNumber[previousRoman]); //Twice because I added wrongly in the previous cycle
                else
                    total += RomanToNumber[currentRoman];

                previousRoman = currentRoman;
            }

            return total;
        }

        public string ToRoman(int number)
        {
            var roman = new StringBuilder();

            foreach (var item in NumberToRoman)
            {
                while (number >= item.Key)
                {
                    roman.Append(item.Value);
                    number -= item.Key;
                }
            }

            return roman.ToString();
        }

        //nejdriv
        public string DecimalToBinaryEasy(int number)
        {
            return Convert.ToString(number, 2);
        }

        public string DecimalToBinary(int number)
        {
            List<int> binaryNumber = new List<int>();
            for (int i = 0; number > 0; i++)
            {
                binaryNumber.Add(number % 2);
                number = number / 2;
            }

            binaryNumber.Reverse();

            return String.Join("", binaryNumber);
        }

        public int BinaryToDecimalEasy(string binaryNumber)
        {
            return Convert.ToInt32(binaryNumber, 2);
        }

        public int BinaryToDecimal(string binaryNumber)
        {
            char[] binArr = binaryNumber.ToCharArray();
            Array.Reverse(binArr);

            double dec = 0;
            for (int i = 0; i < binArr.Length; i++)
            {
                if (binArr[i] == '1')
                    dec += Math.Pow(2, i);
            }

            return (int) dec;
        }

    }
}
