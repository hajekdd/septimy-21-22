A
Na vypracování teoretické části máte 35 minut.
Některé odpovědi se mohou lišit v jiných programovacích jazycích, ptám se na C# a "svět" .NET Frameworku.

Nematuranti nemusí na dvě otázky odpovědět.


a) Převody mezi binární a desítkovou soustavou. Zaznamenejte i postup, aby bylo zřejmé, jak jste k řešení dospěli. (5b)
	1) 189
	2) 101010011
	3) Jak dlouhé bude v binární soustavě číslo 260?

b) Podle čeho dělíme řadící algoritmy? Káždé kriterium vysvětlete a uveďte příklad algoritmu. (5b)

c) Vysvětlete algoritmy pracující na principu Rozděl a panuj a uvďte příklad algoritmu, jenž ho využívá. (5b)

d) Co znamenají zkratky CLI a GUI a jak se liší? Vysvětlete, kdy se hodí co. (5b)

e) Výjmenujte 10 ovládacích prvků a řekněte, co dělají. (5b)

f) Jaké znáte číselné soustavy? Výjmenujte 4 a řekněte, kde nebo k čemu se používají.

g) Co je to dictionary (slovník)?

h) Co je to v OOP Interface a jak se liší od normální třídy? Jak v C# zajistíme dědičnost (třida vs interface)? (5b)





