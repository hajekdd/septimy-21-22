﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stringy2
{
    class Program
    {
        static void Main(string[] args)
        {
            //ZkouskaGenericity();
            //PraceSeStringem();
            //PouzivaniStringBuilder();
            Formatovani();
            
            Console.ReadKey();

        }

        static void Formatovani()
        {
            double pi = Math.PI;
            //pro vlozeni promenne muzeme pouzit nasledujici konstrukci
            string piString1 = $"Pí na 2 desetinná místa je {pi}";
            string piString2 = $"Pí na 2 desetinná místa je {pi:f2}";
            string piString3 = $"Pí na 3 desetinná místa je {pi:f3}";

            string piString2_stejne = string.Format("Pí na 2 desetinná místa je {0:f2}", pi);
            string piString3_stejne = string.Format("Pí na 3 desetinná místa je {0:f3}", pi);


            StringBuilder sb = new StringBuilder();
            DateTime dnes = DateTime.Now;
            DateTime date1 = new DateTime(2008, 8, 29, 19, 27, 15);
            sb.Append("Dnes je ");
            sb.Append($"{dnes}");
            sb.AppendLine();
            sb.Append($"{dnes:F}");
            sb.AppendLine();
            sb.Append(dnes.ToString("F"));
            sb.AppendLine();
            sb.Append("Today is ");
            sb.Append(dnes.ToString("F", System.Globalization.CultureInfo.GetCultureInfo("en-US")));
            sb.AppendLine();
            sb.Append(dnes.ToString("d MMMM", System.Globalization.CultureInfo.GetCultureInfo("en-US"))); //d cislo dne v mesici a MMMM uplny nazev mesice
            sb.AppendLine();
            sb.Append("Dnes je (zjednodušená čínština) ");
            sb.Append(dnes.ToString("F", System.Globalization.CultureInfo.GetCultureInfo("zh-CN")));
            Console.WriteLine(sb);
        }


        static void PouzivaniStringBuilder()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ahoj");
            sb.Append(" svete");
            sb.Append(", jmenuji se");
            sb.AppendLine();
            sb.Append(Environment.NewLine);
            sb.Append("David");

            string text = sb.ToString();
            Console.WriteLine(text);

            sb.Clear();
        }

        static void PraceSeStringem()
        {
            //práce s textem
            string text = "Hello world!";

            //oper8tor + - zretězení
            text = "Hello" + " " + "world";

            char prvniZnak = text[0];   // přístup k řetězci jako ke znakům
            //text[0] = "l"; //readonly
            Console.WriteLine(text.Length);

            foreach (char ch in text)
            {
                Console.WriteLine(ch);
            }

            if (prvniZnak == 'A')
                Console.WriteLine("Začátek abecedy.");

            string text2 = "";
            if (string.IsNullOrEmpty(text2))
                Console.WriteLine("Text je prázdný");

            string text3 = "ahoj";
            if (text3.Equals("ahoj"))
                Console.WriteLine("Texty se rovnají");
            string text4 = null;
            //if (text4.Equals(null))
            //Console.WriteLine("Texty se rovnají (4 - equals)");
            if (text4 == null)
                Console.WriteLine("Texty se rovnají (4 - ==)");
            Console.WriteLine(2 == 2.0);
            Console.WriteLine(2.Equals(2.0));

            //split - Rozdělí řetězec do pole podřetězců, přičemž k zalomení dojde v místě výskytu zadaného znaku (např.  středník)
            string cislaSCarkou = "1,2,3,4,5,6";
            string[] poleCisel = cislaSCarkou.Split(',');
            printArray(poleCisel);

            //trim - ořízne všechny bílé znaky
            Console.WriteLine("trim");
            string smezerami = " můj text s mezerami okolo \n\t";
            string bezmezer = smezerami.Trim();
            Console.WriteLine(smezerami);
            Console.WriteLine(bezmezer);

            // "\n" novy radek a "\t" tabulator
            Console.WriteLine("Ahoj \njak se mas?");
            Console.WriteLine("Ahoj \tjak se mas?");

            //ToLower a ToUpper - převede řetězec na malá/velká písmena
            string pismena = "HelloWorld";
            string malymiPismeny = text.ToLower();
            string velkymiPismeny = text.ToUpper();
            Console.WriteLine(malymiPismeny);
            Console.WriteLine(velkymiPismeny);

            //substring - vytvoří podřetězec SubString(index, length)
            string subText = pismena.Substring(3, 4);
            Console.WriteLine(subText);

            int x = 10;
            Console.WriteLine(x.ToString("Fn"));

        }

        static void ZkouskaGenericity()
        {
            //genericita
            Trida<int> instance = new Trida<int>(10);
            Console.WriteLine(instance.getPromenna());

            List<int> name = new List<int>(); //ctrl + click na List mi ukaze jeho definici

        }


        static void printArray(string[] array)
        {
            foreach (string item in array)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

    }



    public class Trida<T>
    {
        private T promenna;

        public Trida(T prom)
        {
            promenna = prom;
        }

        public T getPromenna()
        {
            return promenna;
        }
    }

}
