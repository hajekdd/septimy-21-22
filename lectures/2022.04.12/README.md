# GIT

## .gitignore

Create a .gitignore file in your Git repo to prevent Git from staging unwanted files.

Share .gitignore in the default branch in your repo.

You and your team can update the file to change which types of files to ignore.

### Co ignorovat?

IDE soubory
Build soubory
Lokální konfigurace
atd..


### example

.vs/

### Šablona

https://github.com/github/gitignore/blob/main/VisualStudio.gitignore


### ignorovat věci, co už jsou přidané

1) Add the file in your .gitignore.

2) git rm --cached  <file>

3) Commit the removal of the file and the updated .gitignore to your repo.



### dokuemntace

https://docs.microsoft.com/en-us/azure/devops/repos/git/ignore-files?view=azure-devops&tabs=command-line


## rebase

- historie commitu

git log

q


- zkontroluj staged

git status


git add .

git commit -m "message"


*nejde pushnout

git pull --rebase origin main

- stáhnu všechny nový commity a moje nový commity hodím na začátek.
