CSV soubor s níže uvedenou strukturou obsahuje na každém svém řádku geometrické údaje o jedne úsečce vektorového obrázku. 

Připravte program, který údaje z tohoto souboru přečte a vykreslí podle nich příslušný obrázek.
