# Numerické metody - určitý integrál

- Spočítejte určitý integrál funkce cos (x) na intervalu <a, b>, kde hodnoty a, b zadá uživatel.
- Nepoužívejte žádné knihovny třetích stran
- Jak lze upravit přesnost?