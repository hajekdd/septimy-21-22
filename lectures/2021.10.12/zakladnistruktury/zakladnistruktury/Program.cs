﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zakladnistruktury
{
    class Program
    {
        static void Main(string[] args)
        {
            // Základem strukturovaného programování jsou:
            // 1) Sekvence
            // 2) Selekce
            // 3) Cyklus


            // Sekvence - posloupnost
            int number = 5;
            number += 5;
            Console.WriteLine("number is " + number);


            // Selekce - podminka, vetveni

            // Zakladni podminka se znaci pomoci klicoveho slova if, za kterym nasleduje logicky vyraz.
            if (10 > 5)
                Console.WriteLine("10 je vetsi nez 5");

            Console.WriteLine();

            // mame nasledujici operatory: 
            // ==  !=   rovnost a nerovnost
            // >   <   <=   >=      mensi/vetsi
            // !    negace


            // klicove slovo else provede kod kdyz podminka neni splnena
            // kod je prehlednejsi a nemusime vymyslet opacnou podminku
            int a = 5;
            int b = 10;

            if (a > b)
                Console.WriteLine("a je vetsi nez b");
            else
                Console.WriteLine("b neni vetsi nez b");

            if (a > b)
            {
                Console.WriteLine("a je vetsi nez b");
                Console.WriteLine("a je vetsi nez b");
            }
            else
                Console.WriteLine("b neni vetsi nez b");


            // podminku muzeme skladat pomoci logicky operatoru
            // && a zaroven
            // || nebo
            Console.WriteLine();
            Console.WriteLine("Logicke operatory && a ||");
            if (a > b && a < b)
                //if (a > b || a < b)
                //if (false || true)
                Console.WriteLine("plati");
            else
                Console.WriteLine("neplati");


            // switch
            // Zjednoduseny zapis vice podminek pod sebou
            Console.WriteLine();
            Console.WriteLine("Zadejte volbu");
            a = 5;
            b = -5;
            int operat = int.Parse(Console.ReadLine());
            float result = 0;

            if (operat == 1)
                result = a + b;
            else if (operat == 2)
                result = a - b;
            else if (operat == 3)
                result = a * b;
            else if (operat == 4)
                result = a / b;
            if ((operat > 0) && (operat < 5))
                Console.WriteLine("Výsledek: {0}", result);
            else
                Console.WriteLine("Neplatná volba");


            switch (operat)
            {
                case 1:
                    result = a + b;
                    break;
                case 2:
                    result = a - b;
                    break;
                case 3:
                    result = a * b;
                    break;
                case 4:
                    result = a / b;
                    break;
            }
            if ((operat > 0) && (operat < 5))
                Console.WriteLine("Výsledek: {0}", result);
            else
                Console.WriteLine("Neplatná volba");





            // iterace - cykly
            // nějaký kód opakujeme
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Cykly");


            // for cyklus
            // zkratka for + 2 * tab
            //stanoveny pevny pocet opakovani
            //promenna, podminka a operace
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            //mala nasobilka
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Console.Write("{0}{1} ", i, j);
                }
                Console.WriteLine();
            }


            Console.WriteLine();
            Console.WriteLine("While cyklus");
            //while cyklus
            //dokud plati podminka tak to opakuje
            int k = 1;
            while (k <= 10)
            {
                Console.Write("{0} ", k);
                k++;
            }
            Console.WriteLine();

            //do-while
            //Cyklus 'do-while' je stejny jako cyklus 'while', akorat ze podminka je vyhodnocena az na konci bloku, ktery nasleduje po klicovem slovu 'do'.
            Console.WriteLine("do-while");
            int l = 11;
            do
            {
                Console.Write("{0} ", l);
                l++;
            } while (l <= 10);
            Console.WriteLine();


            // foreach
            // Cyklus 'foreach' se pouziva k iteraci pres vsechny prvky obsazene v objektu, ktery implementuje rozhrani IEnumerable.
            Console.WriteLine("foreach");
            int[] array = new int[] { 1, 2, 3 };
            //foreach(datovyTyp promenna in array)
            foreach (int o in array)
                Console.Write(o);
            Console.WriteLine();



            // pole
            // slouzi k ochovani vetsiho mnozstvi promennych stejneho typu
            // musime definovat fixni velikost pri deklaraci
            // int[] pole; 
            Console.WriteLine();
            Console.WriteLine("pole");
            int[] pole = new int[10];
            pole[0] = 1;
            for (int i = 0; i < 10; i++)
                pole[i] = i + 1;

            for (int i = 0; i < pole.Length; i++)
                Console.Write("{0} ", pole[i]);
            Console.WriteLine();

            // muzeme rovnou inicializovat pole s konkretnimi hodnotami
            int[] pole2 = new int[] { 1, 2, 3 };
            foreach (int o in pole2)
                Console.Write(o);
            Console.WriteLine();



            // Funkce
            // Metody bez návratové hodnoty
            // Metoda s návratovou hodnotou
            // Metoda s parametrem
            // Metoda s více vstupními parametry
            // volání metody
            Console.WriteLine();
            Console.WriteLine("Funkce");
            Name();
            Console.WriteLine(ReturnInt());
            Console.WriteLine(WithParameter("ahoj"));
            Console.WriteLine(WithParameters("ahoj", 1));
            //this.Name(); //neplati pac jsme ve static

            Console.ReadKey();
        }

        public static void Name()
        {
            Console.WriteLine("Name called");
        }
         
        public static int ReturnInt()
        {
            return 1;
        }
         
        public static int WithParameter(string par1)
        {
            if (par1 == "ahoj")
                return 1;
            else
                return 0;
        }
         
        public static int WithParameters(string par1, int par2)
        {
            if (par1 == "ahoj" && par2 == 1)
                return 1;
            else
                return 0;
        }

    }
}
