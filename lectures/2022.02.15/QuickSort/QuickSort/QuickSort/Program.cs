﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSort
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] arr = new int[] { 38, 27, 43, 3, 9, 82, 10 };
            Console.WriteLine("Given array");
            Console.WriteLine(String.Join(", ", arr));

            Quicksort(arr, 0, arr.Length - 1);

            Console.WriteLine();
            Console.WriteLine("Sorted array");
            Console.WriteLine(String.Join(", ", arr));

            Console.ReadKey();
        }

        // preusporada pole na prvky mensi nez pivot, pivot a prvky vetsi nez pivot
        public static int Divide(int[] arr, int left, int right, int pivot)
        {
            int temp = arr[pivot]; // prohozeni pivotu s poslednim prvkem
            arr[pivot] = arr[right];
            arr[right] = temp;

            int i = left;
            for (int j = left; j < right; j++)
            {
                if (arr[j] < arr[right])
                { // prvek je mensi, nez pivot
                    temp = arr[i]; // prohozeni pivotu s prvkem na pozici
                    arr[i] = arr[j];
                    arr[j] = temp;
                    i++; // posun pozice
                }
            }

            temp = arr[i]; // prohozeni pivotu zpet
            arr[i] = arr[right];
            arr[right] = temp;
            return i; // vrati novy index pivotu
        }

        public static void Quicksort(int[] arr, int left, int right)
        {
            if (right >= left)
            { // podminka rekurze
                int pivot = left; // vyber pivotu
                int new_pivot = Divide(arr, left, right, pivot);
                // rekurzivni zavolani na obe casti pole
                Quicksort(arr, left, new_pivot - 1);
                Quicksort(arr, new_pivot + 1, right);
            }
        }
    }
}
