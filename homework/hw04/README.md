## Vypočítejte n-tý prvek fibonacciho posloupnosti.

Napište dvě metody. Jedna bude počítat n-tý prvek rekurzivně a jedna pomocí cyklu.


Deadline: 09.11.2021

př.:

Jaký prvek fibonacciho posloupnosti chcete vypočítat?
10
Výsledek je 55


Jaký prvek fibonacciho posloupnosti chcete vypočítat?
20
Výsledek je 6765

